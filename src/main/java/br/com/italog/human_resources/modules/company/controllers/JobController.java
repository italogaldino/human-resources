package br.com.italog.human_resources.modules.company.controllers;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.italog.human_resources.modules.company.JobEntity;
import br.com.italog.human_resources.modules.company.dto.createJobDTO;
import br.com.italog.human_resources.modules.company.useCases.CreateJobUseCase;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping("/company")
public class JobController {

    @Autowired
    private CreateJobUseCase createJobUseCase;

    @PostMapping("/job")
    @PreAuthorize("hasRole('COMPANY')")
    public JobEntity create(@Valid @RequestBody createJobDTO createJobDTO, HttpServletRequest request) {

        var jobEntity = JobEntity.builder().companyId(UUID.fromString(request.getAttribute("company_id").toString()))
                .benefits(createJobDTO.getBenefits()).description(createJobDTO.getDescription())
                .level(createJobDTO.getLevel()).build();
        return this.createJobUseCase.execute(jobEntity);
    }

}
