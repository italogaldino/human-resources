package br.com.italog.human_resources.modules.company;

import java.time.LocalDateTime;
import java.util.UUID;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.validator.constraints.Length;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.validation.constraints.Pattern;
import lombok.Data;

@Data
@Entity(name = "company")
public class CompanyEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;
    private String name;

    @Pattern(regexp = "\\S+", message = "O campo (username) não deve conter espaço.")
    private String username;

    @Length(min = 10, max = 100, message = "Senha deve conter entre 10 e 100 caracteres.")
    private String password;
    private String email;
    private String website;
    private String description;

    @CreationTimestamp
    private LocalDateTime createdAt;
}
