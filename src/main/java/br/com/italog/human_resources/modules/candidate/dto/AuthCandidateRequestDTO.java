package br.com.italog.human_resources.modules.candidate.dto;

public record AuthCandidateRequestDTO(String username, String password) {
    
}
