package br.com.italog.human_resources.modules.company.dto;

import lombok.Data;

@Data
public class createJobDTO {
    
    private String description;
    private String benefits;
    private String level;
}
