package br.com.italog.human_resources.modules.company.useCases;

import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;

import javax.security.sasl.AuthenticationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;

import br.com.italog.human_resources.modules.company.CompanyRepository;
import br.com.italog.human_resources.modules.company.dto.AuthCompanyDTO;
import br.com.italog.human_resources.modules.company.dto.AuthCompanyResponseDTO;
import br.com.italog.human_resources.modules.company.dto.AuthCompanyResponseDTO.AuthCompanyResponseDTOBuilder;

@Service
public class AuthCompanyUseCase {

    @Value("${security.token.secret}")
    private String secretKey;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public AuthCompanyResponseDTO execute(AuthCompanyDTO authCompanyDTO) throws AuthenticationException {

        var company = companyRepository.findByUsername(authCompanyDTO.getUsername()).orElseThrow(
                () -> {
                    throw new UsernameNotFoundException("username/password incorrect!");
                });

        // Verificar se senhas são iguais
        var passwordMatches = this.passwordEncoder.matches(authCompanyDTO.getPassword(), company.getPassword());

        // Se senhas não forem iguais, retorna erro
        if (!passwordMatches) {
            throw new AuthenticationException();
        }

        // Se senhas foram iguais retorna token
        Algorithm algorithm = Algorithm.HMAC256(secretKey);
        var expiresIn = Instant.now().plus(Duration.ofMinutes(45));
        var token = JWT.create().withIssuer("javagas").withExpiresAt(Instant.now().plus(java.time.Duration.ofHours(2)))
                .withSubject(company.getId().toString()).withClaim("roles", Arrays.asList("COMPANY")).withExpiresAt(expiresIn).sign(algorithm);

        var authCompanyResponseDTO = AuthCompanyResponseDTO.builder().accessToken(token).expiresIn(expiresIn.toEpochMilli()).build();
        return authCompanyResponseDTO;
    }

}
