package br.com.italog.human_resources.exceptions;

public class UserFoundException extends RuntimeException {

    public UserFoundException() {
        super("Usuário já existe!");
    };
    
}
